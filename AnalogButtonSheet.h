#ifndef AnalogButtonSheet_h
#define AnalogButtonSheet_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
#endif

class AnalogButtonSheet
{      
    public:        
        AnalogButtonSheet(int interval_up, int interval_down);
        int get_interval_up();
        int get_interval_down();
    private:
        int interval_up;
        int interval_down;
};

#endif // AnalogButtonSheet_h
