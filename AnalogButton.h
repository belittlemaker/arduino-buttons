#ifndef AnalogButton_h
#define AnalogButton_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
#endif

#include "AnalogButtonSheet.h"

#define ANALOG_BUTTON_NON_PUSHED -1

class AnalogButton
{      
    public:
        AnalogButton(int pin, int lenght);
        ~AnalogButton();
        void add_button(int interval_up, int interval_down);
        int get_pushed();
    private:
        int lenght;
        int pin;
        int state;
        AnalogButtonSheet* buttons_sheet;
        int get_pushed_sheet();
        int read_input();
};

#endif // AnalogButton_h
