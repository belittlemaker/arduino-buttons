# Arduino Buttons

Support libraries to drive buttons through digital or analogue pins

Tutorial
--------

1. AnalogButton
```c
#include "AnalogButton.h"

#define A_BTTN_PIN     A4
#define A_BTTN_LENGHT  2

AnalogButton* analog_button;

void setup()
{
    Serial.begin(9600);

    analog_button = new AnalogButton(A_BTTN_PIN, A_BTTN_LENGHT);
    analog_button->add_button(1010, 995);
    analog_button->add_button(515, 510);
}

void loop()
{
    switch (analog_button->get_pushed()) {
      case 1:
        Serial.println("Pushed Button: 1");
        break;
      case 2:
        Serial.println("Pushed Button: 2");
        break;
    }
}
```

2. DigitalButton
```c
#include "DigitalButton.h"

#define D_BTTN_PIN  8

DigitalButton* digital_button;

void setup()
{
    Serial.begin(9600);

    digital_button = new DigitalButton(D_BTTN_PIN);
}

void loop()
{
    if (digital_button->is_pushed()) {
        Serial.println("Pushed button");
    }
}
```
