#include "AnalogButton.h"

AnalogButton::AnalogButton(int pin, int lenght)
{
    this->lenght = lenght;
    this->pin = pin;
    this->state = ANALOG_BUTTON_NON_PUSHED;
    
    this->buttons_sheet = (AnalogButtonSheet*)malloc(sizeof(AnalogButtonSheet) * lenght);
}

AnalogButton::~AnalogButton()
{
    free(this->buttons_sheet);
}

void AnalogButton::add_button(int interval_up, int interval_down)
{
    static int pos = 0;

    if (pos < this->lenght) {
        this->buttons_sheet[pos] = *(new AnalogButtonSheet(interval_up, interval_down));
        pos++;
    }
}

int AnalogButton::get_pushed_sheet()
{
    int input = (int)this->read_input();
    int pushed = ANALOG_BUTTON_NON_PUSHED;
    
    for (int index = 0; index < this->lenght; index++) {
        AnalogButtonSheet buttons_sheet = this->buttons_sheet[index];

        if ((buttons_sheet.get_interval_up() > input) && (input >= buttons_sheet.get_interval_down())) {
            pushed = index + 1;
            break;
        }
    }

    return pushed;
}

int AnalogButton::get_pushed()
{
    int pushed_button = this->get_pushed_sheet();

    int pushed = ANALOG_BUTTON_NON_PUSHED;
    if ((pushed_button != ANALOG_BUTTON_NON_PUSHED) && (pushed_button != this->state)) {
        pushed = pushed_button;
    }

    this->state = pushed_button;

    return pushed;
}

int AnalogButton::read_input()
{
    return analogRead(this->pin);
}
