#include "AnalogButtonSheet.h"

AnalogButtonSheet::AnalogButtonSheet(int interval_up, int interval_down)
{
    this->interval_up = interval_up;
    this->interval_down = interval_down;
}

int AnalogButtonSheet::get_interval_up()
{
    return this->interval_up;
}

int AnalogButtonSheet::get_interval_down()
{
    return this->interval_down;
}
