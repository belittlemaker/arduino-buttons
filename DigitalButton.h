#ifndef DigitalButton_h
#define DigitalButton_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
    #else
    #include <WProgram.h>
#endif

class DigitalButton
{      
    public:        
        DigitalButton(int pin);
        bool is_pushed();
    private:
        int pin; 
        int state;
        int read_state();
};

#endif // DigitalButton_h
