#include "DigitalButton.h"

DigitalButton::DigitalButton(int pin)
{
    this->pin = pin;
    this->state = LOW;
}

bool DigitalButton::is_pushed()
{
    int state_new = this->read_state();

    bool pushed = state_new == HIGH && this->state == LOW;
    
    this->state = state_new;

    return pushed;
}

int DigitalButton::read_state()
{
    return digitalRead(this->pin);
}
